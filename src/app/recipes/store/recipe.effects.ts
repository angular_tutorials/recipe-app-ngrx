import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest } from '@angular/common/http';

import { Effect, Actions } from '@ngrx/effects';
import { Store } from '@ngrx/store';

import { switchMap, map, withLatestFrom } from 'rxjs/operators';

import * as RecipeActions from '../store/recipe.actions';
import * as fromRecipe from '../store/recipe.reducers';
import { Recipe } from '../recipe.model';

@Injectable()
export class RecipeEffects {
  @Effect()
  recipeFetch = this.actions$
    .ofType(RecipeActions.FETCH_RECIPES)
    .pipe(
      switchMap((action: RecipeActions.FetchRecipes) => {
        return this.httpClient.get<Recipe[]>('https://ng-recipe-book-86ca0.firebaseio.com/recipes.json');
        // this.httpClient.get('https://ng-recipe-book-86ca0.firebaseio.com/recipes.json?auth=' + token, {
        //   observe: 'body',
        //   responseType: 'json'
        // })
      }
      ),
      map(
        (recipes) => {
          //   console.log(recipes);
          for (const recipe of recipes) {
            if (!recipe['ingredients']) {
              recipe['ingredients'] = [];
            }
          }
          return {
            type: RecipeActions.SET_RECIPES,
            payload: recipes
          };
          // return recipes;
          //   return [];
        }
      )
    );

  @Effect({dispatch: false})
  recipeStore = this.actions$
    .ofType(RecipeActions.STORE_RECIPES)
    .pipe(
      // combines the value of ofType with this value
      withLatestFrom(this.store.select('recipes')),
      switchMap(([action, state]) => {
        const req = new HttpRequest(
          'PUT',
          'https://ng-recipe-book-86ca0.firebaseio.com/recipes.json',
          state.recipes,
          // { reportProgress: true, params: params }
          { reportProgress: true }
        );

        return this.httpClient.request(req);
      }
      )
    );


  constructor(
    private actions$: Actions,
    private httpClient: HttpClient,
    private store: Store<fromRecipe.FeatureState>
  ) { }
}
