import * as RecipeActions from './recipe.actions';

import { Recipe } from '../recipe.model';
import { Ingredient } from '../../shared/ingredient.model';

import * as fromApp from '../../store/app.reducers';

// extend fromApp to use the states of the app
export interface FeatureState extends fromApp.AppState {
  recipes: State;
}

export interface State {
  recipes: Recipe[];
}

const initialState: State = {
  recipes: [
    new Recipe('A Test Recipe',
     'Simply a test',
     'https://upload.wikimedia.org/wikipedia/commons/1/15/Recipe_logo.jpeg',
    [
      new Ingredient('Meat', 3),
      new Ingredient('Pomems', 4),
    ]),
    new Recipe('Another Test Recipe',
     'Simply a test',
      'https://upload.wikimedia.org/wikipedia/commons/1/15/Recipe_logo.jpeg',
    [
      new Ingredient('Meat', 3),
      new Ingredient('Pomems', 4),
    ]),
    new Recipe('Lasagne',
     'Rezept für Lasagne',
      'https://upload.wikimedia.org/wikipedia/commons/6/6e/Tuna_lasagne_for_lunch%2C_January_2011.jpg',
    [
      new Ingredient('Zwiebel', 1),
      new Ingredient('Knoblauchzehe', 1),
      new Ingredient('Olivenöl, EL', 2),
      new Ingredient('Rindshackfleisch, Gramm', 500),
      new Ingredient('Tomaten, Gramm', 500),
    ]),
  ]
};

export function recipeReducer(state = initialState, action: RecipeActions.RecipeActions) {
  switch (action.type) {
    case (RecipeActions.SET_RECIPES):
      return {
        ...state,
        recipes: [...action.payload]
      };
    case (RecipeActions.ADD_RECIPE):
      return {
        ...state,
        recipes: [...state.recipes, action.payload]
      };
    case (RecipeActions.UPDATE_RECIPE):
      const recipe = state.recipes[action.payload.index];
      // Using object spread, you can merge both objects into a single new object
      // Object spread will create a new object, copy over all property values from 'recipe',
      // and then copy over all property values from 'action.payload.updatedRecipe' — in that order, from left to right.
      const updatedRecipe = {
        ...recipe,
        ...action.payload.updatedRecipe
      };
      const recipes = [...state.recipes];
      recipes[action.payload.index] = updatedRecipe;
      return {
        ...state,
        recipes: recipes
      };
      case (RecipeActions.DELETE_RECIPE):
        const oldRecipes = [...state.recipes];
        oldRecipes.splice(action.payload, 1);
        return {
          ...state,
          recipes: oldRecipes
        };
      default:
        return state;
  }

}
