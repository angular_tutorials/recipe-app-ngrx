import { Component, OnInit, OnDestroy } from '@angular/core';
import * as firebase from 'firebase';
import { Store } from '@ngrx/store';

import { take } from 'rxjs/operators';

import * as fromApp from './store/app.reducers';
import * as AuthActions from './auth/store/auth.actions';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {
  subscription: Subscription;

  constructor(private store: Store<fromApp.AppState>) { }

  ngOnInit() {
    firebase.initializeApp({
      apiKey: 'AIzaSyDPQZGxHs1U9MQnq6ZkuhDUQWVWE_nhtMY',
      authDomain: 'ng-recipe-book-86ca0.firebaseapp.com',
    });

  const unsubscribe = firebase.auth().onAuthStateChanged((user) => {
  if (user) {
    this.subscription = this.store.select('auth')
      .pipe(
        take(1)
      )
      .subscribe((data) => {
        if (!data.authenticated) {
          user.getIdToken().then(
            (token: string) => {
              this.store.dispatch(new AuthActions.GetToken(token));
              unsubscribe();
            }
          );
        } else {
          unsubscribe();
        }
      });
  }
});

  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
