import { Component, OnInit, OnDestroy } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Store } from '@ngrx/store';
import { Router } from '@angular/router';

import { Subscription } from 'rxjs';
import { take } from 'rxjs/operators';

import * as fromApp from '../../store/app.reducers';
import * as AuthActions from '../../auth/store/auth.actions';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit, OnDestroy {
  subscription: Subscription;

  constructor(
    private store: Store<fromApp.AppState>,
    private router: Router,
  ) { }

  ngOnInit() {
    this.subscription = this.store.select('auth')
      .pipe(
        take(1)
      )
      .subscribe(
      data => {
        if (data.authenticated) {
          this.router.navigate(['/']);
        }
    });
  }

  onSignup(form: NgForm) {
    const email = form.value.email;
    const password = form.value.password;

    this.store.dispatch(new AuthActions.TrySignup({username: email, password: password}));
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
